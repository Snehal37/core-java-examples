package com.example.Excel_data.service;

import java.io.IOException;
import java.util.List;

import com.example.Excel_data.entity.Employee;

public interface ExcelService {

	List<Employee> Readdata() throws IOException;

}
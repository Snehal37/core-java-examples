package com.example.Excel_data.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.example.Excel_data.entity.Employee;

@Service
public class ExcelServiceImpl implements ExcelService {

	@Override

	public List<Employee> Readdata() throws IOException {

		List<Employee> list = new ArrayList<Employee>();
		FileInputStream inputStream = new FileInputStream(new File("C:\\JAVA_TEAM\\Excel\\users.xlsx"));
		Workbook workbook = new XSSFWorkbook(inputStream);
		Sheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowiterator = sheet.iterator();

		// User user =new User();
		rowiterator.next();
		while (rowiterator.hasNext()) {
			Row row = rowiterator.next();
			System.out.println(row.toString());
		Employee employee = new Employee();
		if(row.getCell(0).getCellType()==CellType.NUMERIC) {
			employee.setSrNo(row.getCell(0).getNumericCellValue());
		}

		if (row.getCell(1).getCellType() == CellType.STRING) {
			employee.setRewardType(row.getCell(1).getStringCellValue());
		}
		if (row.getCell(2).getCellType() == CellType.STRING) {
			employee.setEmployeeName(row.getCell(2).getStringCellValue());
		}

		list.add(employee);

		}

		return list;
	} 

	

}

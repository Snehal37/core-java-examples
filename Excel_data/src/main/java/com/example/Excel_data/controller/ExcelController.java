package com.example.Excel_data.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import com.example.Excel_data.entity.Employee;
import com.example.Excel_data.service.ExcelServiceImpl;

@Controller
public class ExcelController {

	@Autowired
	private ExcelServiceImpl excelServiceImpl;

	@GetMapping("/getdata")
	public String Readdata(Model model) throws Exception {
		model.addAttribute("employee", new Employee());
		List<Employee> data = excelServiceImpl.Readdata();
		model.addAttribute("data", data);

		return "employees";

	}

}

package com.example.Excel_data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcelDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExcelDataApplication.class, args);
	}

}

package com.example.StudentData.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.StudentData.Entity.Student;

public interface StudentSer extends CrudRepository<Student, Integer>{

}

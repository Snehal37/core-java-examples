package com.example.JpaDemo.entity;
import javax.persistence.Column;  
import javax.persistence.Entity;  
import javax.persistence.Id;  
import javax.persistence.Table;

import lombok.Data;  
  @Data
@Entity  
@Table  
public class Books  
{   
@Id  
@Column  
private int bookid;  
@Column  
private String bookname;  
@Column  
private String author;  
@Column  
private int price;  

}  
package com.example.JpaDemo.repo;
import org.springframework.data.repository.CrudRepository;

import com.example.JpaDemo.entity.Books;  

//repository that extends CrudRepository  
public interface BooksRepository extends CrudRepository<Books, Integer>  
{  
}  
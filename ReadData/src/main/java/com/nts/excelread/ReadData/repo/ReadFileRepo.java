package com.nts.excelread.ReadData.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nts.excelread.ReadData.entity.User;

public interface ReadFileRepo extends JpaRepository<User,Long> {

}

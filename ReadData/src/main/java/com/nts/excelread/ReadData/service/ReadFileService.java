package com.nts.excelread.ReadData.service;


import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.nts.excelread.ReadData.entity.User;


public interface ReadFileService {

	List<User> findAll();

	boolean saveDataFromUploadfile(MultipartFile file);

}

package com.nts.excelread.ReadData.service;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.nts.excelread.ReadData.entity.User;
import com.nts.excelread.ReadData.repo.ReadFileRepo;

@Service
@Transactional
public class ReadFileServiceImpl implements ReadFileService{
	@Autowired
	private ReadFileRepo readFileRepo;

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return (List<User>) readFileRepo.findAll();
	}

	@Override
	public boolean saveDataFromUploadfile(MultipartFile file) {
		// TODO Auto-generated method stub
		boolean isFlag=false;
		String extension=FilenameUtils.getExtension(file.getOriginalFilename());
		if(extension.equalsIgnoreCase("xls") || extension.equalsIgnoreCase("xlsx")) {
			isFlag= readDataFromExcel(file);
		}
		return false;
	}

	private boolean readDataFromExcel(MultipartFile file) {
		Workbook workbook=getWorkBook(file);
		
		Sheet sheet =workbook.getSheetAt(0);
		Iterator<Row> rows =sheet.iterator();
		rows.next();
		while(rows.hasNext()) {
			Row row=rows.next();
			User user= new User();
			if(row.getCell(0).getCellType()==CellType.STRING) {
				user.setFirstName(row.getCell(0).getStringCellValue());
			}
			if(row.getCell(1).getCellType()== CellType.STRING) {
				user.setLastName(row.getCell(1).getStringCellValue());
			}
			if(row.getCell(2).getCellType()== CellType.STRING) {
				user.setEmail(row.getCell(2).getStringCellValue());
			}
			if(row.getCell(3).getCellType()==CellType.NUMERIC) {
				String phoneNumber=NumberToTextConverter.toText(row.getCell(3).getNumericCellValue());
				user.setPhoneNumber(phoneNumber);
				
			}else if(row.getCell(3).getCellType()== CellType.STRING) {
				user.setPhoneNumber(row.getCell(3).getStringCellValue());
			}
			user.setFileType(FilenameUtils.getExtension(file.getOriginalFilename()));
			readFileRepo.save(user);
		}
		
		
		return true;
	}

	private Workbook getWorkBook(MultipartFile file) {
		Workbook workbook=null;
		String extension = FilenameUtils.getExtension(file.getOriginalFilename());
		try {
			if(extension.equalsIgnoreCase("xlsx")) {
				workbook=new XSSFWorkbook(file.getInputStream());
				
			}else if(extension.equalsIgnoreCase("xls")) {
				workbook=new HSSFWorkbook(file.getInputStream());
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return workbook;
	}
}

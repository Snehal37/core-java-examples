package com.examaple.task;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class SortHashMap {
  public static void main(String[] args) {
    
    Map<String, String> map = new HashMap<String, String>();
   
    map.put("101", "Snehal");
    map.put("103", "Komal");
    map.put("112", "Shraddha");
    map.put("115", "Vishal");
    
   
   
    System.out.println("-- Unsorted Map --");
    for(Map.Entry<String, String> emp : map.entrySet()) {
      System.out.println("Key- " + emp.getKey() + 
                  " Value- " + emp.getValue());
    }
       
    TreeMap<String, String> sortedEmpMap = new TreeMap<>(map);
    System.out.println("-- Map sorted by keys --");
    for(Map.Entry<String, String> emp : sortedEmpMap.entrySet()) {
      System.out.println("Key - " + emp.getKey() + 
             " Value- " + emp.getValue());
    }
  }
}

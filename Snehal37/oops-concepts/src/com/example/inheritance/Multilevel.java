package com.example.inheritance;
class Level1{
String a = "Hello";
void Display() {
    System.out.println("Hi \t"+a);
    System.out.println("This is Class1");
}
}

class Level2 extends Level1{
String b = "Welcome";
void Display2() {
    System.out.println("please \t"+b);
    System.out.println(" This is Clase2");
}
}

public class Multilevel extends Level2{

public static void main(String[] args) {
Multilevel c = new Multilevel();

System.out.println("Call  of Main method");
c.Display();
c.Display2();  
}
}
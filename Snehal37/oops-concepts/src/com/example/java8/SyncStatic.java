package com.example.java8;

class Demo1{  
	  
	 synchronized static void printTable(int n){  
	   for(int i=1;i<=10;i++){  
	     System.out.println(n*i);  
	     try{  
	       Thread.sleep(400);  
	     }catch(Exception e){}  
	   }  
	 }  
	}  
	  
	class Fun extends Thread{  
	public void run(){  
	Demo1.printTable(1);  
	}  
	}  
	  
	class Gun extends Thread{  
	public void run(){  
	Demo1.printTable(10);  
	}  
	}  
	  
	class Sun extends Thread{  
	public void run(){  
	Demo1.printTable(100);  
	}  
	}  
	  
	  
	  
	  
	class Mun extends Thread{  
	public void run(){  
	Demo1.printTable(1000);  
	}  
	}  
	  
	public class SyncStatic{  
	public static void main(String t[]){  
	Fun t1=new Fun();  
	Gun t2=new Gun();  
	Sun t3=new Sun();  
	Mun t4=new Mun();   	
	t1.start();  
	t2.start();  
	t3.start();  
	t4.start();  
	}  
	}  
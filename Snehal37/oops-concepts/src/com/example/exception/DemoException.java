package com.example.exception;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DemoException {
	public static void main(String args[]) {
		int i,j=0,k=0;
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));	
	i=8;
	
	try {
		System.out.println("Enter Number");
		j=Integer.parseInt(br.readLine());
		k=i/j;
		System.out.println("Output is: "+k);
		
		}
	catch(IOException e) 
	{
		System.out.println("Some IO Error");
	}catch(ArithmeticException e) 
	{
		System.out.println("Cannot Divide by zero"+e);
	}
	catch(Exception e) {
		System.out.println("Unknown Exxception");
	}finally {
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Finally Done");
	}
	}
}

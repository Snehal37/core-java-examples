package com.example.abstraction;

abstract class Animal {
  
  public abstract void animalSound();
 
  public void Drink() {
    System.out.println("Cats have Milk in their food");
  }
}


class Cat extends Animal {
  public void animalSound() {
  
    System.out.println("The cat says: Meow Meow");
  }
}

class Test {
  public static void main(String[] args) {
    Cat cat = new Cat(); 
    cat.animalSound();
    cat.Drink();
  }
}